# CarCar

Team:

* Patrick - Sales
* Tyler - Service

## Design
The overall design of this application was to provide a service to a car dealership/sales/service. You can store all your data for the sales and the services and access them in a very user friendly interface.
## Service microservice

In the back-end development, I created a Service and Technician model that displays technicians name, employee number and will use that in the Service model that displays a description
of the service performed. The AutomobileVO class I created will poll information from the Inventory to get the VIN number which will be displayed on the various pages in the front-end. I also created views and encoders that perform various api request to get, pull, delete, and create information from our back-end database. One feature I added was the VIP feature in my service encoder. This takes a key called "vip" that has a value of true if the object has a vin that is associated with where the car was sold. So if the customer brings their car into the our dealership for service and we can see that their VIN matches a VIN in our sales history, this customer will receive VIP service treatement. Another important api request I made was to get all this service appointments that are "finished" and places them into our service history. It gets removed from the front-end service appointment list page, but stay in back-end data. I used this to build my service history front-end page, which has a search feature built in to search a finished service appointment based on the VIN. Also I added a delete feature which allows us to delete a service appointment enitrely. On the front-end page I created various data fetches from our local server that parses the data into JSON format that is used to update the state variables. I then displayed this data on various pages for user and client to see. These include creating a technician, creating an appointment, vehicle model, manufacturer, and automobile.

## Sales microservice

In the django backend I created models for an automobileVO that captures the VIN and sold attributes from Automobile model in the inventory microservice using a poller. I also created models and views to create new salespeople and customers so a salesperson can create a new sale with an existing employee and customer added to the drop down menus of the form. All the create forms were made via react and es6 to post data into json back into the api sales microservice.
