from django.shortcuts import render
from .models import AutomobileVO, SalesPerson, Customer, AutoSale
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "id",
        ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "address",
        "phone_number",
    ]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "employee_name",
        "id",
    ]

class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "employee_name",
        "employee_number",
    ]

class AutoSaleListEncoder(ModelEncoder):
    model = AutoSale
    properties = [
        "id",
        "price",
        "sales_person",
    ]

    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "customer": o.customer.customer_name,
            "sales_person": [o.sales_person.employee_name, o.sales_person.employee_number]
        }

class AutoSaleDetailEncoder(ModelEncoder):
    model = AutoSale
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "price",
    ]
    encoders= {
        "automobile": AutomobileVODetailEncoder(),
        "sales_person": SalesPersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }




@require_http_methods(["GET", "POST"])
def api_list_customers(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def api_list_sales_people(request):
    if request.method == "GET":

        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonListEncoder
        )
    else:
        content = json.loads(request.body)
        sales_people = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_people,
            encoder=SalesPersonDetailEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_sales_person(request, pk):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=pk).update(**content)
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_auto_sales(request, automobile_vo_id=None):

    if request.method == "GET":
        auto_sales = AutoSale.objects.all()
        return JsonResponse(
            {"auto_sales": auto_sales},
            encoder=AutoSaleDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.filter(vin=automobile_vin, sold = False)
            content["automobile"] = automobile

            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "automobile does not exist"},
                status=400,
            )
        auto_sales = AutoSale.objects.create(**content)
        return JsonResponse(
            auto_sales,
            encoder=AutoSaleDetailEncoder,
            safe=False
        )




@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_auto_sales(request, pk):
    if request.method == "GET":
        auto_sale = AutoSale.objects.get(id=pk)
        return JsonResponse(
            auto_sale,
            encoder=AutoSaleDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = AutoSale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        AutoSale.objects.filter(id=pk).update(**content)
        auto_sale = AutoSale.objects.get(id=pk)
        return JsonResponse(
            auto_sale,
            encoder=AutoSaleDetailEncoder,
            safe=False,
        )
