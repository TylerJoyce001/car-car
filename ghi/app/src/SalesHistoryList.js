import React from 'react';

class SalesHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            salesperson_id: '',
            auto_sales: [],
            allAutoSales: [],
        }
    }
    async componentDidMount() {
        const response = await fetch("http://localhost:8090/api/auto_sales/")
        if (response.ok) {
            const data = await response.json()
            this.setState({auto_sales: data.auto_sales})
            this.setState({allAutoSales: data.auto_sales})
        }
    }

    handleSearch = (event) => {
        const value = (event.target.value);
        this.setState({ salesperson_id: value })
    }

    handleFilter = () => {
        const salesPersonId = this.state.salesperson_id;
        const allAutoSales = this.state.allAutoSales;
        if (salesPersonId !== "") {
            const selectedSalesperson = this.auto_sales.filter((sales) => salesPersonId === sales.sales_person.employee_number)
            this.setState({ auto_sales: selectedSalesperson })
        } else {
            this.setState({ auto_sales: allAutoSales })
        }
    }

    render() {
        return (
            <>
                <div className="my-3 container">
                        <div className="my-3 container">
                            <form onSubmit={this.handleFilter} id="search-history-form" className="form-inline" style={{width: '100%'}}>
                                <select onChange={this.handleSearch} value={this.state.employee_number} placeholder="Name" required type ="text" name="salesperson" id="salesperson" className="form-control">
                                    <option value="">Choose A Sales Person</option>
                                    {this.state.auto_sales.map(salesman => {
                                        return (
                                            <option key={salesman.sales_person.employee_number} value={salesman.sales_person.employee_number}> {salesman.sales_person.employee_name}</option>
                                        )
                                    })}
                                </select>
                            </form>
                        </div>
                        <h1>Sales Person History</h1>
                        <table className="table table-striped">
                            <thead>
                            <tr>
                                <th>Sales Person</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sale Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.auto_sales.map(sales => {
                                const price = sales.price.toLocaleString('en-US', {
                                    style: 'currency',
                                    currency: 'USD',
                                    });
                            return (
                            <tr key={sales.href}>
                                <td>{ sales.sales_person.employee_name }</td>
                                <td>{ sales.customer.customer_name }</td>
                                <td>{ sales.automobile.vin }</td>
                                <td>{ price }</td>
                            </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </>
        );
    }
}
export default SalesHistoryList;
