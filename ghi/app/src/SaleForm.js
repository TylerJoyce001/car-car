import React from 'react';

class SaleForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobile: '',
            automobiles: [],
            sales_person: '',
            sales_persons: [],
            customer: '',
            customers: [],
            price: ''
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state }

        const url = "http://localhost:8090/api/auto_sales/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch (url, fetchOptions);
        if (response.ok) {

        }
        this.setState({
            automobile: '',
            sales_person: '',
            customer: '',
            price: ''
        })
    }
    handleAutomobile = (event) => {
        const value = event.target.value;
        this.setState({ automobile: value })
    }
    handleSalesPerson = (event) => {
        const value = event.target.value;
        this.setState({ sales_person: value })
    }
    handleCustomer = (event) => {
        const value = event.target.value;
        this.setState({ customer: value })
    }
    handlePrice = (event) => {
        const value = event.target.value;
        this.setState({ price: value })
    }

    loadAutomobile = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/");

        if (response.ok) {
            const data = await response.json();

            this.setState({automobiles: data.autos})
        }
    }
    loadSalesPerson = async () => {
        const response = await fetch("http://localhost:8090/api/sales_people/");

        if (response.ok) {
            const data = await response.json();
            this.setState({sales_persons: data.sales_people})
        }
    }
    loadCustomer = async () => {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (response.ok) {
            const data = await response.json();
            this.setState({customers: data.customers})
        }
    }

    async componentDidMount() {
        this.loadCustomer();
        this.loadAutomobile();
        this.loadSalesPerson();
    }


    render() {
        console.log(this.state)
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Sale Form</h1>
                        <form onSubmit={this.handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <select onChange={this.handleAutomobile} value={this.state.automobile} placeholder="Name" required type ="text" name="automobile" id="automobile" className="form-control">
                                <option value="">Choose an Automobile</option>
                                {this.state.automobiles.map(automobile =>{
                                    return (
                                        <option key={automobile.id} value={automobile.id}> {automobile.vin}</option>
                                    )
                                })}
                                </select>
                                <select onChange={this.handleSalesPerson} value={this.state.sales_person} placeholder="Name" required type ="text" name="sales_person" id="sales_person" className="form-control">
                                <option value="">Sales Person</option>
                                {this.state.sales_persons.map(sales_person =>{
                                    return (
                                        <option key={sales_person.id} value={sales_person.id}> {sales_person.employee_name}</option>
                                    )
                                })}
                                </select>
                                <select onChange={this.handleCustomer} value={this.state.price} placeholder="Name" required type ="number" name="customer" id="customer" className="form-control">
                                <option value="">Customer</option>
                                {this.state.customers.map(customer =>{
                                    return (
                                        <option key={customer.id} value={customer.id}> {customer.customer_name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePrice} value={this.state.reason} placeholder="Name" required type ="number" name="reason" id="reason" className="form-control" />
                                <label htmlFor="name">Price</label>
                            </div>
                            <button className="btn btn-primary btn-lg">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default SaleForm;
