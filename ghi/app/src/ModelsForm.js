import React from 'react';

class ModelsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            models: [],
            color: '',
            year: '',
            vin: '',
            model_id: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }
    async componentDidMount () {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json()
            this.setState({models: data.models})
        }
    }

    handleInput = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        if (name === 'model') {
            this.setState({ 'model_id': value })
        } else {
            this.setState({ [name]: value })
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        delete data.models;
        const url = 'http://localhost:8100/api/automobiles/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            this.setState({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add an automobile to inventory</h1>
                        <form onSubmit={this.handleSubmit} id="create-model-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInput} value={this.state.color} placeholder="Name" required type="text" name="color" id="color" className="form-control" maxLength="50" />
                                <label htmlFor="name">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInput} value={this.state.year} placeholder="Name" required type="text" name="year" id="year" className="form-control" />
                                <label htmlFor="name">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInput} value={this.state.vin} placeholder="Name" required type="text" name="vin" id="vin" className="form-control" maxLength="17" />
                                <label htmlFor="name">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={this.handleInput} value={this.state.id} placeholder="Name" required type="text" name="model" id="model" className="form-control">
                                    <option value="">Choose A Model</option>
                                    {this.state.models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>{model.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary btn-lg">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default ModelsForm;
