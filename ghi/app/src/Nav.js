import React from "react";
import { NavLink } from "react-router-dom";
import Logo from "./images/Logo.png";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-light">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          <img className="navbar-brand" src={Logo} to="/" alt="Brand" />
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-dark"
                data-bs-toggle="dropdown"
                href="@"
                role="button"
                aria-expanded="true"
              >
                Inventory
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdown"
                data-bs-auto-close="true"
              >
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/">
                    Manufacturers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/new">
                    Create A Manufacturer
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/models/">
                    Vehicle Models
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/models/new">
                    Create Vehicle Model
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles/">
                    Automobile List
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles/new">
                    Create Automobile
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-dark"
                data-bs-toggle="dropdown"
                href="@"
                role="button"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdown"
                data-bs-auto-close="true"
              >
                <li>
                  <NavLink className="dropdown-item" to="SalesPersonForm">
                    Create Sales Person Form
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="CustomerForm">
                    Create Customer Form
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="SalesList">
                    List of Sales
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="SaleForm">
                    New Sale Form
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/SalesHistoryList">
                    Sales History List
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle text-dark"
                data-bs-toggle="dropdown"
                href="@"
                role="button"
                aria-expanded="false"
              >
                Service
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdown"
                data-bs-auto-close="true"
              >
                <li>
                  <NavLink className="dropdown-item" to="/technician/">
                    Add A Technician
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointment/">
                    List of Appointments
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointment/new">
                    Schedule An Appointment
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointment/history">
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
