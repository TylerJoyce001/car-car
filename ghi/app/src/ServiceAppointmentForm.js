import React from 'react';

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: '',
            owner: '',
            appointment: '',
            technicians: [],
            technician: '',
            reason: '',

        }
        // this.handleSubmit = this.handleSubmit.bind(this)
        // this.handleOwner = this.handleOwner.bind(this)
        // this.handleAppointment = this.handleAppointment.bind(this)
        // this.handleTechnician = this.handleTechnician.bind(this)
        // this.handleReason = this.handleReason.bind(this)
    }
    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state }
        delete data.technicians

        const url = "http://localhost:8080/api/services/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch (url, fetchOptions);
        if (response.ok) {

        const newAppointment = await response.json();

        }
        this.setState({
            vin: '',
            owner: '',
            appointment: '',
            technician: '',
            reason: '',
        })
    }
    handleVin = (event) => {
        const value = event.target.value;
        this.setState({ vin: value })
    }
    handleOwner = (event) => {
        const value = event.target.value;
        this.setState({ owner: value })
    }
    handleAppointment = (event) => {
        const value = event.target.value;
        this.setState({ appointment: value })
    }
    handleTechnician = (event) => {
        const value = event.target.value;
        this.setState({ technician: value })
    }
    handleReason = (event) => {
        const value = event.target.value;
        this.setState({ reason: value })
    }
    async componentDidMount() {
        const url = "http://localhost:8080/api/technician/";

        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Service Appointment Form</h1>
                        <form onSubmit={this.handleSubmit} id="create-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleVin} value={this.state.vin} placeholder="Name" required type ="text" name="vin" id="vin" className="form-control" maxLength="17" />
                                <label htmlFor="name">Vin Number</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleOwner} value={this.state.owner} placeholder="Name" required type ="text" name="owner" id="owner" className="form-control" />
                                <label htmlFor="name">Car Owner</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleAppointment} value={this.state.appointment} placeholder="Name" required type ="datetime-local" name="apppointment" id="apppointment" className="form-control" />
                                <label htmlFor="name">Appointment Time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={this.handleTechnician} value={this.state.technician} placeholder="Name" required type ="text" name="technician" id="technician" className="form-control">
                                <option value="">Choose a Technician</option>
                                {this.state.technicians.map(technician =>{
                                    return (
                                        <option key={technician.id} value={technician.id}> {technician.technician_name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleReason} value={this.state.reason} placeholder="Name" required type ="text" name="reason" id="reason" className="form-control" />
                                <label htmlFor="name">Reason</label>
                            </div>
                            <button className="btn btn-primary btn-lg">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default AppointmentForm;
