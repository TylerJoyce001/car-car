import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import AppointmentForm from './ServiceAppointmentForm';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistoryList';
import ManufacturerList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import ModelsList from './ModelsList';
import ModelsForm from './ModelsForm';
import SaleForm from './SaleForm';
import SalesHistoryList from './SalesHistoryList';
import './main.css';

function App() {
	return (
		<BrowserRouter>
			<Nav />

			<Routes>
				<Route path="/" element={<MainPage />} />
				<Route path="/SalesPersonForm" element={<SalesPersonForm />} />
				<Route path="/CustomerForm" element={<CustomerForm />} />
				<Route path="/SalesList" element={<SalesList />} />
				<Route path="/technician/" element={<TechnicianForm />} />
				<Route path="/appointment/" element={<AppointmentList />} />
				<Route path="/appointment/new" element={<AppointmentForm />} />
				<Route path="/appointment/history" element={<ServiceHistory />} />
				<Route path="/manufacturers" element={<ManufacturerList />} />
				<Route path="/manufacturers/new" element={<ManufacturerForm />} />
				<Route path="/models/" element={<AutomobileList />} />
				<Route path="/models/new" element={<AutomobileForm />} />
				<Route path="/automobiles/" element={<ModelsList />} />
				<Route path="/automobiles/new" element={<ModelsForm />} />
				<Route path="/SaleForm" element={<SaleForm />} />
				<Route path="/SalesHistoryList" element={<SalesHistoryList />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;
