import React from 'react';


class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        employeeName: '',
        employeeNumber: '',
        salesPeople: [],
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
        employee_name: this.state.employeeName,
        employee_number: this.state.employeeNumber,
        };


        const salesPeopleUrl = 'http://localhost:8090/api/sales_people/';
        const fetchOptions = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json'
        },
        };

        const salesPeopleResponse = await fetch(salesPeopleUrl, fetchOptions);
        if (salesPeopleResponse.ok) {
        const newsalesPerson = await salesPeopleResponse.json();

        this.setState({
            employeeName:'',
            employeeNumber: '',
        });
        }

    }
        handleEmployeeNameChange = (event) => {
            const value = event.target.value;
            this.setState({employeeName: value})
    }
        handleEmployeeNumberChange = (event) => {
            const value = event.target.value;
            this.setState({employeeNumber: value})
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add New Sales Person</h1>
                        <form onSubmit={this.handleSubmit} id="create-salesPerson-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmployeeNameChange} value={this.state.employeeName} placeholder="Name" required type="text" name="employeeName" id="employeeName" className="form-control" />
                                <label htmlFor="name">Employee Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmployeeNumberChange} value={this.state.employeeNumber} placeholder="Name" required type="text" name="employeeNumber" id="employeeNumber" className="form-control" />
                                <label htmlFor='name'>Employee Number</label>
                            </div>
                                <button className='btn btn-lg btn-primary'>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default SalesPersonForm;
