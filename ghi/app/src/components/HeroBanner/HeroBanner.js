import './HeroBanner.css';

export const HeroBanner = (props) => <div className="hero-banner">{props.children}</div>;
