import React from 'react';
import { NavLink } from 'react-router-dom';
import HeroBanner from './components/HeroBanner';
import Logo from './images/ServiCarMainPage.png';

function MainPage() {
	return (
		<div className="hero-wrapper">
			<HeroBanner>
				<img className="navbar-brand-main" src={Logo} to="/" alt="Brand" />
				<p className="lead mb-4">The premiere solution for automobile dealership management!</p>
			</HeroBanner>
		</div>
	);
}

export default MainPage;
