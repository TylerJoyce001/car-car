import React from "react";

class TechnicianForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      technicianName: "",
      employeeNumber: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleNumberChange = this.handleNumberChange.bind(this);
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      technician_name: this.state.technicianName,
      employee_number: Number(this.state.employeeNumber),
    };
    const techUrl = "http://localhost:8080/api/technician/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const techResponse = await fetch(techUrl, fetchOptions);
    if (techResponse.ok) {
      const newTech = await techResponse.json();
    }
    this.setState({
      technicianName: "",
      employeeNumber: "",
    });
  };
  handleNameChange = (event) => {
    const value = event.target.value;
    this.setState({ technicianName: value });
  };
  handleNumberChange = (event) => {
    const value = event.target.value;
    this.setState({ employeeNumber: value });
  };
  async componentDidMount() {
    const url = "http://localhost:8080/api/technician/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ technician: data.technician });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Technician Form</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.technicianName}
                  placeholder="Name"
                  required
                  type="text"
                  name="technicianName"
                  id="technicianName"
                  className="form-control"
                />
                <label htmlFor="name">Technician Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNumberChange}
                  value={this.state.employeeNumber}
                  placeholder="Name"
                  required
                  type="text"
                  name="employeeNumber"
                  id="employeeNumber"
                  className="form-control"
                />
                <label htmlFor="name">Employee Number</label>
              </div>
              <button className="btn btn-primary btn-lg">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianForm;
