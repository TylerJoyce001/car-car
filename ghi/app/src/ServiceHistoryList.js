import React from "react";

class AppointmentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      services: [],
      allServices: [],
    };
  }
  async componentDidMount() {
    const response = await fetch("http://localhost:8080/api/services/history/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ services: data.history });
      this.setState({ allServices: data.history });
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const vin = this.state.vin;
    const allServices = this.state.allServices;
    if (vin !== "") {
      const selectedService = allServices.filter(
        (service) => vin === service.vin
      );
      this.setState({ services: selectedService });
    } else {
      this.setState({ services: allServices });
    }
  };

  handleSearch = (event) => {
    const value = event.target.value;
    this.setState({ vin: value });
  };

  render() {
    return (
      <>
        <div className="my-3 container">
          <form
            onSubmit={this.handleSubmit}
            id="search-history-form"
            className="form-inline"
          >
            <div
              className="form-floating mb-3 input-group"
              style={{ width: "100%" }}
            >
              <input
                onChange={this.handleSearch}
                value={this.state.vin}
                id="search"
                type="search"
                placeholder="Search"
                aria-label="Search"
                style={{ width: "90%" }}
              />
              <button
                id="searchBtn"
                className="btn btn-outline-success"
                type="submit"
                style={{ width: "10%", padding: 5 }}
              >
                Search VIN
              </button>
            </div>
          </form>
          <h1>Service Appointments</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN#</th>
                <th>Customer name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
              </tr>
            </thead>
            <tbody>
              {this.state.services.map((service) => {
                const dateTime = new Date(service.appointment);
                return (
                  <tr key={service.vin}>
                    <td>{service.vin}</td>
                    <td>{service.owner}</td>
                    <td>{dateTime.toLocaleDateString()}</td>
                    <td>
                      {dateTime.toLocaleTimeString([], {
                        hour: "2-digit",
                        minute: "2-digit",
                      })}
                    </td>
                    <td>{service.technician.technician_name}</td>
                    <td>{service.reason}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  }
}
export default AppointmentList;
